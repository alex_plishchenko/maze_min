require "utility"
require "solver"
require "LeeAlgorithm"

require "config"

fileList = {
	"test\\Maze_2-1.txt",
}

function startMsg(index)
	local line = ''
	local cout = 50

	for i=1, cout do  line = line..'-' end
	print( line )
	print("Test:" , index)
	print( line )
end

function endMsg()
	local line = '';
	for i=1, 50 do
		line = line..'='
	end
	print( line )
end


for i, fileName in ipairs( fileList ) do

	tableWord = {}

	startMsg(i)

	loadFile(fileName, tableWord)

	print2d(tableWord)

	if not Solver:initWorld(tableWord) then
		LeeAlgorithm:init(tableWord, config, Solver.start, Solver.finish)

		Solver:init(LeeAlgorithm)
		Solver:run()

		Solver:solverToFile(fileNameOut, fileName)	
	end
	endMsg()
end

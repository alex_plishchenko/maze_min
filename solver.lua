require "utility"

require "config"

Solver = {
	world = {},
	path = {},
	algorithm = {},
	start = {x, y},
	finish = {x, y},
}

function Solver:init(algorithm)
	self.algorithm = algorithm
end

function Solver:run(algorithm)
	self.algorithm:run()
	self.path = self.algorithm:getPath()
end

function Solver:getPath()
	return self.path
end

function Solver:initWorld(world)
	self.world = world

	self.finish.x, self.finish.y = fineFinish(self.world, config["Finish"])
	self.start.x, self.start.y = fineStart(self.world, config["Start"])

	if(self.start.x == nil) or (self.finish.x == nil) then
		print( "Error: initWorld. when find Start or Finish!")
		return true
	else
		print( "Start:", '['.. self.start.x..',' ..self.start.y..']')
		print( "Finish:", '['.. self.finish.x..',' ..self.finish.y..']')
	end
end


function Solver:solverToFile(outFileName, inFileName, mode)
	local tableWord2 = {}

	local index = 1
	for line in io.lines(inFileName) do
		tableWord2[index] = {}     
		for j = 1, #line do
			tableWord2[index][j] = atChar(line, j)	

			for i, v in ipairs( self.path ) do
				if(v[1] == index and  v[2] == j) then
					tableWord2[index][j] = config["Path"]
					break
				end
			end

		end
		 index = index + 1
	end

	print2d(tableWord2)

	if (mode == true) then
		writeToFile(outFileName, tableWord2)
	end
end

function writeToFile(outFileName, tableWord)
	fileOut = io.open(outFileName, "w")

	io.output(fileOut)

	for i = 1, #tableWord do
		local line = ""
		for j = 1, #tableWord[i] do
			line = line .. tableWord[i][j]
		end
		io.write(line)
		if (i < #tableWord) then
			io.write('\n')
		end
	end

	io.close(fileOut)
end

function fineFinish(world, simbol)
	for i = 1, #world do
		for j = 1, #world[i] do
			if (world[i][j] == simbol) then
				return i, j
			end
		end
	end
	return nil
end

function fineStart(world, simbol)
	for i = 1, #world do
		for j = 1, #world[i] do
			if (world[i][j] == simbol) then
				return i, j
			end
		end
	end
	return nil
end


function loadFile(inFileName, world)
	local index = 1
	for line in io.lines(inFileName) do
		world[index] = {}     
		for j = 1, #line do
			world[index][j] = atChar(line, j)
		end
		 index = index + 1
	end

	assert(isValidWorld(world), "Error:World - invalid size")
end


function isValidWorld(world)
	local LenMax = #world[1];
	for i = 1, #world do
		if  not (LenMax == #world[i])  then
			print( "isValidWorld: size = ",  LenMax, #world[i])
			return false
		end
	end
	return true
end

LeeAlgorithm = {
	world = {},
	path = {},
	config = {},
	start  = {x, y},
	finish = {x, y},
 }

function LeeAlgorithm:init(world, configWorld, start, finish)
	self.world  = world
	self.config = config
	self.start = start
	self.finish = finish
end

function LeeAlgorithm:getPath()
	return self.path
end

function LeeAlgorithm:run()
	self:speadWave()
	self:restorPath()
end

function LeeAlgorithm:speadWave()

	self.world[self.finish.x][self.finish.y] = self.config["Empty"]
	self.world[self.start.x][self.start.y] = 1

	local distance = 1

	while true do
		local stop = 1
	      for i = 1, #self.world do
	      	local line = ""
	      		for j = 1, #self.world[i] do
	      			if (self.world[i][j] == distance) then
						-- right
						if (j + 1 <= #self.world[i]) and (self.world[i][j + 1] == self.config["Empty"]) then
							self.world[i][j + 1] = distance + 1;
							stop = 0;
						end
						-- left
						if (j - 1 > 0 ) and (self.world[i][j - 1] == self.config["Empty"]) then
							self.world[i][j - 1] = distance + 1;
							stop = 0
						end
						-- down
						if (i + 1 <= #self.world) and (self.world[i + 1][j] == self.config["Empty"]) then
							self.world[i + 1][j] = distance + 1;
							stop = 0
						end
						-- up
						if  (i - 1 > 0)  and (self.world[i - 1][j] == self.config["Empty"]) then
							self.world[i - 1][j] = distance + 1;
							stop = 0
						end
	      			end
	      		end
	      end
	      distance = distance + 1
	      if (stop == 1) and (self.world[self.finish.x][self.finish.y] == self.config["Empty"]) then
	      		print( "stop:", stop )
	      		break;
	      end
	      if not(self.world[self.finish.x][self.finish.y] == self.config["Empty"]) then
	      	print( stop )
	      		break;
	      end
	end

	if self.world[self.finish.x][self.finish.y] == self.config["Empty"] then
	      print( "Error: not found Path to Finish.(")
	end
end

function LeeAlgorithm:restorPath()

	local x = self.finish.y
	local y = self.finish.x
	
	local distance = self.world[self.finish.x][self.finish.y];

	print( "len:",  distance)

	local indexPath = 0;

	while (distance > 2) do
		distance = distance - 1
		indexPath = indexPath + 1
		-- up
		if (y - 1) > 0 and (self.world[y - 1][x] == distance) then
			y = y - 1
			self.path[indexPath] = {y, x}
		end
		-- down
		if (y + 1) <= #self.world and (self.world[y + 1][x] == distance) then
			y = y + 1
			self.path[indexPath] = {y, x}
		end
		-- left
		if (x - 1 > 0) and (self.world[y][x - 1] == distance) then
			x = x - 1
			self.path[indexPath] = {y, x}
		end		
		-- right
		if (x + 1 < #self.world[1]) and (self.world[y][x + 1] == distance) then
			x = x + 1
			self.path[indexPath] = {y, x}
		end			
	end
end
require "utility"
require "solver"
require "LeeAlgorithm"

require "config"

fileName = "Maze.txt"
fileNameOut = "MazeSolver.txt"

tableWord = {}

loadFile(fileName, tableWord)

print2d(tableWord)

if not Solver:initWorld(tableWord) then
	LeeAlgorithm:init(tableWord, config, Solver.start, Solver.finish)

	Solver:init(LeeAlgorithm)
	Solver:run()

	Solver:solverToFile(fileNameOut, fileName, true)	
end


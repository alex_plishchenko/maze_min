

function showFile( fileName )
	local index = 0
	for line in io.lines(fileName) do
		index = index + 1
	 	print(line, #line, "index:", index)
	end
end

function print2d(matrix)
	print(" ")
	for i, v in ipairs( matrix ) do
		local line = ""
		for i = 1, #v do
			line = line .. v[i].." "
		end
		print( line )
	end
	print(" ")
end

function atChar(str, inxex)
	if (inxex == 0) then return nil end
	return string.char(str:byte(inxex))
end
